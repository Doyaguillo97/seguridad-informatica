
#include <AES.h>
#include <ArduinoSTL.h>
 
AES aes ;
 
unsigned int keyLength [3] = {128, 192, 256}; // longitud de la clave: 128b, 192b or 256b
 
byte *key = (unsigned char*)"01234567890123456789012345678901"; // encryption key
byte plain[] = "Texto muy secreto"; 
// plaintext to encrypt
 
unsigned long long int myIv = 36753562; // CBC vector de inicializacion; real iv = iv x2 ex: 01234567 = 0123456701234567
 
void setup (){
  
  Serial.begin (9600);
}
 
void loop () 
{
  for (int i=0; i < 3; i++){// va pasando por todas las longitudes
    
    Serial.print("- key length [b]: ");
    Serial.println(keyLength [i]);
    aesTest (keyLength[i]);
    delay(2000);
  }
  delay(10000);
}
 
void aesTest (int bits)
{
  aes.iv_inc();
  
  byte iv [N_BLOCK] ;
  int plainPaddedLength = sizeof(plain) + (N_BLOCK - ((sizeof(plain)-1) % 16)); // length of padded plaintext [B]
  byte cipher [plainPaddedLength]; // mensaje cifrado
  byte check [plainPaddedLength]; // mensaje descifrado
  aes.set_IV(myIv);
  aes.get_IV(iv);
 
  Serial.println("- tiempo en encriptar: ");
  unsigned long ms = micros ();
  aes.do_aes_encrypt(plain,sizeof(plain),cipher,key,bits,iv);//encriptamos el texto
  Serial.println(micros() - ms);
 
  aes.set_IV(myIv);
  aes.get_IV(iv);
  
  Serial.println("- tiempo en desencrptar: ");
  ms = micros ();
  aes.do_aes_decrypt(cipher,aes.get_size(),check,key,bits,iv); 
  Serial.println(micros() - ms);
  
  Serial.print("- plain:   ");
  aes.printArray(plain,(bool)true); //print plain with no padding
 
  Serial.print("- cipher:  ");
  aes.printArray(cipher,(bool)false); //print cipher with padding
 
  Serial.print("- check:   ");
  aes.printArray(check,(bool)true); //print decrypted plain with no padding
  
  Serial.println("- iv:      ");
  aes.printArray(iv,16); //print iv
  printf("\n===================================================================================\n");
  
}
